import os


ROOT_DIR = os.path.dirname(os.path.abspath(__file__))
LIBS_DIR = os.path.join(ROOT_DIR, 'libs')
CONFIG_PATH = os.path.join(LIBS_DIR, 'config.ini')
INITS_DIR = os.path.join(ROOT_DIR, 'inits')
INITS_FILES = os.path.join(INITS_DIR, 'files')