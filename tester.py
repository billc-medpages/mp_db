from components.email import Email
from components.country import Country
from components.service import Service
from inits import read_services_file
from inits import initialization




def main():
    initialization.init()
    s = Service()
    s.set_property_nodes_by_value(value='Cardiology')
    print(s.property_nodes)


if __name__ == '__main__':
    main()