from libs import extensions
from libs import tools
from py2neo import Node, Relationship, NodeMatcher


class MP_Node:
    """The parent Node

    This is the parent node, which will point to the root node and all the property nodes

    Attributes:
        graph (:obj: `neo4j.graph`): pointer to neo4j graph object
        root_label (str): the label of the root node, used for searching, value is set from config file
        property_label (str): the primary label of the property node, used for searching, value is set from config file
        root_node (:obj: `neo4j.Node`): pointer to neo4j node, which is the root node
        property_nodes(:obj: `list of neo4j.Node`): pointer to list of neo4j.Node, which are the property nodes
        config_name (str): the section name in config file
        class_name_singular (str): the class name in singular
        class_name_plural (str): the class name in plural
        property_node_labels (:obj: `list of str`): all the labels of the property node
    """

    graph = None
    root_label = ''
    property_label = ''
    root_node = None
    property_nodes = []
    config_name = ''
    class_name_singular = ''
    class_name_plural = ''
    property_node_labels = []

    def get_root_node(self):
        matcher = NodeMatcher(self.graph)
        root_node = matcher.match(self.root_label, node_type='root_node').first()
        self.root_node = root_node

    def add_root_node(self):
        tx = self.graph.begin()
        root_node = Node(self.root_label, node_type='root_node')
        tx.create(root_node)
        tx.commit()
        if root_node:
            self.root_node = root_node

    def add_property_node(self, **kwargs):
        values = []
        labels = []
        properties = {}

        for k, v in kwargs.items():
            if k == 'value':
                temp = str(v).lower()
                values.append(temp)
            elif k == self.class_name_singular:
                temp = str(v).lower()
                values.append(temp)
            elif k == 'values':
                temps = v
                values = [temp.lower() for temp in temps]
            elif k == self.class_name_plural:
                temps = v
                values = [temp.lower() for temp in temps]
            else:
                properties[k] = v

        properties['node_type'] = 'property_node'
        properties['values'] = values

        tx = self.graph.begin()

        property_node = Node(*self.property_node_labels, **properties)
        tx.create(property_node)

        if not self.root_node:
            self.get_root_node()

        rel_label = 'root_node_{}_link'.format(self.class_name_singular)
        rel = Relationship(self.root_node, rel_label, property_node)
        tx.create(rel)

        tx.commit()

        self.property_nodes = [property_node]

    def set_property_nodes_by_value_query(self, **kwargs):
        sub_query = ''
        values = []
        if self.class_name_singular in kwargs:
            temp = str(kwargs[self.class_name_singular]).lower()
            values.append(temp)
        elif 'value' in kwargs:
            temp = str(kwargs['value']).lower()
            values.append(temp)
        elif self.class_name_plural in kwargs:
            temps = kwargs[self.class_name_plural]
            values = [temp.lower() for temp in temps]
        elif 'values' in kwargs:
            temps = kwargs['values']
            values = [temp.lower() for temp in temps]

        # contruct the query sentence
        for value in values:
            sub_query += '("{}" in n.values) OR '.format(value.lower())
        # take out the latest OR
        sub_query = sub_query.strip().rstrip('OR').strip()

        query = "MATCH (n:{}) WHERE ({}) AND n.node_type = 'property_node' RETURN (n)".format(self.property_label, sub_query)

        return query

    def set_property_nodes_by_value(self, **kwargs):
        query = self.set_property_nodes_by_value_query(**kwargs)
        print(query)
        cursor = self.graph.run(query)
        results = []
        while cursor.forward():
            n = cursor.current['n']
            results.append(n)
        if results:
            self.property_nodes = results

    def get_values_of_property_nodes(self):
        for property_node in self.property_nodes:
            return property_node['values']

    def set_values_of_property_nodes(self, **kwargs):
        values = []
        # default value is append
        mode = 'a'
        if 'value' in kwargs:
            temp = str(kwargs['value']).lower()
            values.append(temp)
        elif 'values' in kwargs:
            temps = kwargs['values']
            values = [temp.lower() for temp in temps]
        elif self.class_name_singular in kwargs:
            temp = str(self.class_name_singular).lower()
            values.append(temp)
        elif self.class_name_plural in kwargs:
            temps = kwargs[self.class_name_plural]
            values = [temp.lower() for temp in temps]
        if 'mode' in kwargs:
            mode = kwargs['mode']

        tx = self.graph.begin()
        current_values = set()
        new_values = set()
        for property_node in self.property_nodes:
            current_values.clear()
            new_values.clear()
            current_values = set(property_node['values'])
            if mode.lower() == 'r':
                print(values)
                print(new_values)
                new_values = set(values)
            elif mode.lower() == 'a':
                new_values = current_values.union(set(values))
            property_node['values'] = list(new_values)
            tx.graph.push(property_node)
        tx.commit()

    def __init__(self, config_name, class_name_singular, class_name_plural, extra_property_node_labels):
        """initialize the object

        :param config_name: the section name in config file
        :param class_name_singular: the class name in singular
        :param class_name_plural: the class name in plural
        :param extra_property_node_labels: additional property node labels, add to self.property_node_labels
        """
        self.config_name = config_name
        self.class_name_singular = class_name_singular
        self.class_name_plural = class_name_plural

        self.graph = extensions.neo4j.get_graph()

        config = tools.read_config_file()
        self.root_label = str(config[self.config_name]['root_label'])
        self.property_label = str(config[self.config_name]['property_label'])

        if extra_property_node_labels:
            self.property_node_labels = [extra_property_node_label.lower() for extra_property_node_label in extra_property_node_labels]
        self.property_node_labels.append(self.property_label)


        self.get_root_node()
        if not self.root_node:
            self.add_root_node()

