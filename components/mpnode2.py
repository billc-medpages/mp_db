from libs import extensions
from libs import tools
from py2neo import Node, Relationship, NodeMatcher


class MPNode():
    """ The parent class of Node Link



    """
    graph = None
    root_label = ''
    property_label = ''
    root_node = None
    property_nodes = []
    config_name = ''
    class_name_singular = ''
    class_name_plural = ''
    property_node_labels = []

    def add_root_node(self, **kwargs):
        labels = []
        properties = {}
        labels.append(self.root_label)
        properties['node_type'] = 'root_node'
        for k, v in kwargs.items():
            properties[k] = v

        tx = self.graph.begin()
        root_node = Node(*labels, **properties)
        tx.create(root_node)
        tx.commit()
        if root_node:
            self.root_node = root_node

    def set_root_node(self, **kwargs):
        values = []
        mpcode = ''
        query = ''
        subquery = ''

        for k, v in kwargs.items():
            if k == 'value':
                value = str(v).lower()
                values.append(value)
            if k == 'values':
                temps = v
                values = [str(temp).lower() for temp in temps]
            if k == 'mpcode' or v == 'medpagescode':
                mpcode = v

        if mpcode:
            query = "MATCH(r:{})--(p:{}) WHERE p.mpcode = '{}' AND p.node_type = 'property_node' return (r)".format(self.root_label, self.property_label, mpcode)

            cursor = self.graph.run(query)
            results = []
            while cursor.forward():
                r = cursor.current['r']
                results.append(r)
            if results:
                self.property_nodes = results[0]
            else:
                # create a new root node
                self.add_root_node()
        elif values:
            for value in values:
                

    def add_property_node(self, **kwargs):
        if not self.root_node:
            self.set_root_node(**kwargs)

        values = set()
        for k, v in kwargs.items():
            if k == 'values':














    def __init__(self):
        class_name = type(self).__name__
        self.root_label = "root_node_{}".format(str(class_name).lower())
        self.property_label = 'property_node_{}'.format(str(class_name).lower())





