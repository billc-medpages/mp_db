from components.mpnode import MP_Node


class Service(MP_Node):

    def set_property_nodes_by_mpcode(self, mpcode):
        """ Medthod used to search by MP Code

        :param mpcode: unique mpcode
        :return: None
        """
        query = "MATCH (n:service) WHERE n.mpcode={x} AND n.node_type={y} RETURN (n) LIMIT 1"
        print(query)
        g = self.g
        cursor = g.run(query, x=str(mpcode), y='property_node')
        results = []
        while cursor.forward():
            n = cursor.current['n']
            results.append(n)
        if results:
            self.property_nodes = results

    def __init__(self):
        super().__init__('Service', 'service', 'services', None)