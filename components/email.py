from components.mpnode import MP_Node


class Email(MP_Node):

    def __init__(self):
        super().__init__('Email', 'email', 'emails', ['e-mail'])