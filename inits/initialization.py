from libs import extensions
from libs import tools
from inits import populate_service_nodes


def init():
    graph = extensions.neo4j.get_graph()
    # wipe out all the data
    graph.delete_all()

    populate_service_nodes.populate_service()
