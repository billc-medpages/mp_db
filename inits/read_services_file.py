import pandas as pd
import os
import definitions


def f(x, y, z):
    value = {}
    value.clear()
    value['medpagescode'] = x
    value['description_eng'] = y
    value['description_fr'] = z
    return value.copy()


def read_services():
    data_file = os.path.join(definitions.INITS_FILES, 'services.xlsx')
    df = pd.read_excel(data_file)
    results = [f(x, y, z) for x, y, z in zip(df['mpcode'], df['clientdescription_eng'], df['clientdescription_fr'])]
    return results
