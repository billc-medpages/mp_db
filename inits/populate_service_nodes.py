import definitions
import pandas as pd
import os
from inits.read_services_file import read_services
from components.service import Service


def populate_service():
    services = read_services()
    values = set()

    for service in services:
        values.clear()
        mpcode = ''

        if 'description_eng' in service:
            values.add(service['description_eng'])
        if 'description_fr' in service:
            values.add(service['description_fr'])
        if 'description_medpages' in service:
            values.add(service['description_medpages'])
        if 'medpagescode' in service:
            mpcode = service['medpagescode']

        s = Service()
        s.add_property_node(values=values, mpcode=mpcode)
