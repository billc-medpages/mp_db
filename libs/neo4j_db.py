from py2neo import Graph


class Neo4j:
    def __init__(self):
        self.graph = None

    def connect(self):
        self.graph = Graph(host='localhost', user='neo4j', password='deathknight')
        return self.graph

    def get_graph(self):
        if not self.graph:
            return self.connect()
        return self.graph