import definitions

import configparser


def read_config_file():
    config = configparser.ConfigParser()
    config.read(definitions.CONFIG_PATH)
    result = {}
    sections = config.sections()
    temp = {}

    for section in sections:
        temp.clear()
        for key in config[section]:
            value = config[section][key]
            temp[key] = value
            result[section] = temp.copy()

    return result
